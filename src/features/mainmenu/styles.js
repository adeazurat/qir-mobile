'use strict';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  icon: {
    marginLeft: 10,
    marginRight: 10,
    paddingBottom: 20,
    paddingTop: 20,
  },
  shortcutContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
    paddingVertical: 16,
  },
});
