'use strict';
import NetInfo from '@react-native-community/netinfo';
import {Spinner, Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {FlatList} from 'react-native';
import {getData, Status} from '../../utils';
import SymposiumCard from './SymposiumCard';

class SymposiaListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: props.data ? Status.SUCCESS : Status.OFFLINE,
      data: props.data ? props.data : [],
    };
  }

  componentDidMount() {
    if (this.state.status !== Status.SUCCESS) {
      NetInfo.isConnected.fetch().then((isConnected) => {
        if (isConnected) {
          this.setState({status: Status.LOADING});
          getData(this.props.endpoint).then((json) => {
            console.log('Received list of symposium as JSON', json);

            let data = json.data;
            if (this.props.poster) {
              data = data.filter((e) => e.isPoster === 'TRUE');
            } else {
              data = data.filter((e) => e.isPoster === 'FALSE');
            }

            this.setState({
              status: Status.SUCCESS,
              data: data,
            });
          }).catch((error) => {
            console.warn(error);
            this.setState({status: Status.ERROR});
          });
        }
      });
    }
  }

  componentWillUnmount() {
    this.setState({data: []});
  }

  render() {
    if (this.state.status === Status.LOADING) {
      return <Spinner />;
    } else if (this.state.status === Status.SUCCESS) {
      return this.renderSymposiaList();
    } else if (this.state.status === Status.OFFLINE) {
      return <Text>You are offline!</Text>;
    } else {
      return <Text>An error has occurred!</Text>;
    }
  }

  renderSymposiaList() {
    const {push} = this.props.navigation;

    return <FlatList keyExtractor={(item, _) => String(item.symposium)}
      data={this.state.data}
      renderItem={(symposium) => <SymposiumCard
        title={symposium.item.title}
        onPress={() => push('SubmissionList',
            {track: symposium.item.symposium})} />}
    />;
  }
};

SymposiaListContainer.propTypes = {
  navigation: PropTypes.object.isRequired,
  data: PropTypes.arrayOf(PropTypes.object),
  poster: PropTypes.bool,
  endpoint: PropTypes.string,
};

SymposiaListContainer.defaultProps = {
  poster: false,
  endpoint: 'https://asia-northeast1-qir-ftui.cloudfunctions.net/sessions',
};

export default SymposiaListContainer;
