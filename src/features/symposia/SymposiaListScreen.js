'use strict';
import React from 'react';
import {Button, Container, Content, Header, Icon,
  Input, Item} from 'native-base';
import PropTypes from 'prop-types';
import {TextHeader2} from '../../components';
import SymposiaListContainer from './SymposiaListContainer';
import styles from './styles';

class SymposiaListScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {query: ''};
  }

  render() {
    const {navigation} = this.props;
    return (
      <Container>
        <Header hasTabs style={styles.header}>
          <TextHeader2 first="Parallel" second="Sessions" />
        </Header>
        <Content>
          <Item underline>
            <Input style={styles.input}
              placeholder="Search by author, paper, keywords, or room"
              placeholderTextColor="gray"
              onChangeText={(text) => this.setState({query: text})}
              onSubmitEditing={() => {
                navigation.push('SearchResultScreen',
                    {query: this.state.query});
              }} />
            <Button style={styles.search} transparent small onPress={() => {
              navigation.push('SearchResultScreen',
                  {query: this.state.query});
            }}>
              <Icon name="magnifier" type="SimpleLineIcons"/>
            </Button>
          </Item>
          <SymposiaListContainer navigation={this.props.navigation} />
        </Content>
      </Container>
    );
  }
}

SymposiaListScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default SymposiaListScreen;
