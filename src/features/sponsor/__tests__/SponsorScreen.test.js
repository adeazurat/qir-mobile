'use strict';
import React from 'react';
import 'react-native';
import TestRenderer from 'react-test-renderer';
import SponsorScreen from '../SponsorScreen';

describe('A sponsor screen component', () => {
  it('matches with the snapshot', () => {
    const mockNavigation = {push: jest.fn()};
    const component = TestRenderer.create(
        <SponsorScreen navigation={mockNavigation}/>
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
