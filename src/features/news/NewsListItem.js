'use strict';
import {Icon} from 'native-base';
import React from 'react';
import {TouchableHighlight, View} from 'react-native';
import {Text} from 'native-base';
import PropTypes from 'prop-types';
import styles from './styles';
import qir from '../../../native-base-theme/variables/qir';

const NewsListItem = ({headline, date, link, navigation}) =>
  <TouchableHighlight underlayColor={qir.brandLighter}
    onPress={() => navigation.push('NewsDetailScreen', {
      uri: link,
    })}>
    <View style={styles.listItemContainer}>
      <View style={styles.listItemLeftColumn}>
        <Text style={styles.date}>{date}</Text>
        <Text style={styles.headline}>{headline}</Text>
      </View>
      <View style={styles.listItemRightColumn}>
        <Icon style={styles.listItemIcon} type="SimpleLineIcons"
          name='arrow-right' />
      </View>
    </View>
  </TouchableHighlight>;

NewsListItem.propTypes = {
  date: PropTypes.string.isRequired,
  headline: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  navigation: PropTypes.object.isRequired,
};

export default NewsListItem;
