'use strict';
import NetInfo from '@react-native-community/netinfo';
import {Spinner, Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {StyleSheet} from 'react-native';
import {parse} from 'react-native-rss-parser';
import {Status} from '../../utils';
import NewsList from './NewsList';

class NewsListContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state= {
      status: Status.OFFLINE,
      data: [],
    };
  }

  componentDidMount() {
    // TODO Get news data from cache if exists
    // Otherwise, fetch it from the Internet
    NetInfo.isConnected.fetch().then((isConnected) => {
      if (isConnected) {
        this.setState({state: Status.LOADING});
        this.getNewsFeedData(this.props.newsFeedUrl).then((json) => {
          console.log('Received news feed data as JSON', json);
          const newsListData = this.prepareNewsListData(json);
          this.setState({
            status: Status.SUCCESS,
            data: newsListData,
          });
        }).catch((error) => {
          console.warn(error);
          this.setState({status: Status.ERROR});
        });
      }
    });
  }

  componentWillUnmount() {
    // TODO Store news data into cache
    this.setState({data: []});
  }

  prepareNewsListData(rss) {
    return rss.items.map((item, index) => {
      return {
        date: item.published,
        headline: item.title,
        link: item.links[0].url,
      };
    });
  }

  getNewsFeedData(url) {
    console.log(`Fetching news feed data from ${url}`);

    return fetch(url, {
      method: 'GET',
      cache: 'default',
      referrer: 'no-referrer',
    }).then((response) => response.text())
        .then((rssXml) => parse(rssXml))
        .catch((error) => {
          console.warn(error);
          throw new Error(`Unable to get news feed data from ${url}`);
        });
  }

  render() {
    if (this.state.status === Status.LOADING) {
      return <Spinner />;
    } else if (this.state.status === Status.SUCCESS) {
      return <NewsList navigation={this.props.navigation}
        newsData={this.state.data} />;
    } else if (this.state.status === Status.OFFLINE) {
      return <Text style={styles.error}>You are offline!</Text>;
    } else {
      return <Text style={styles.error}>An error has occurred!</Text>;
    }
  }

  setStatus(status) {
    this.setState({status: status});
  }
}

NewsListContainer.propTypes = {
  navigation: PropTypes.object,
  newsFeedUrl: PropTypes.string.isRequired,
};

NewsListContainer.defaultProps = {
  newsFeedUrl: 'https://qir.eng.ui.ac.id/category/news/feed',
};

const styles = StyleSheet.create({
  error: {
    marginTop: 16,
    textAlign: 'center',
  },
});

export default NewsListContainer;
