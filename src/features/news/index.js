'use strict';
import NewsListContainer from './NewsListContainer';
import NewsDetailScreen from './NewsDetailScreen';

export {NewsListContainer, NewsDetailScreen};
