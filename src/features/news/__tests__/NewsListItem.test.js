'use strict';
import React from 'react';
import 'react-native';
import {shallow} from 'enzyme';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import NewsListItem from '../NewsListItem';

describe('A list item', () => {
  let mockNavigation = null;
  let fakeDate = null;

  beforeEach(() => {
    mockNavigation = {push: jest.fn()};
    fakeDate = new Date(Date.UTC(2018, 0, 10));
  });

  it('matches its snapshot correctly', () => {
    const component = TestRenderer.create(
        <NewsListItem category="News"
          date={fakeDate.toUTCString()}
          headline="Breaking News!"
          link="http://localhost"
          navigation={mockNavigation} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('can be clicked', () => {
    const wrapper = shallow(
        <NewsListItem category='News'
          date={fakeDate.toUTCString()}
          headline='Breaking News!'
          link='http://localhost'
          navigation={mockNavigation} />
    );

    wrapper.simulate('press');

    expect(mockNavigation.push).toBeCalled();
    expect(mockNavigation.push.mock.calls[0][0]).toBe('NewsDetailScreen');
    expect(mockNavigation.push.mock.calls[0][1]).toEqual({
      uri: 'http://localhost',
    });
  });
});
