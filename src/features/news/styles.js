'use strict';
import {StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

export default StyleSheet.create({
  date: {
    color: qir.brandPrimary,
    textTransform: 'uppercase',
    fontSize: 12,
  },
  headline: {
    fontWeight: 'bold',
  },
  listItemContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 8,
  },
  listItemIcon: {
    fontSize: 16,
  },
  listItemLeftColumn: {
    flex: 3,
    flexDirection: 'column',
  },
  listItemRightColumn: {
    alignItems: 'flex-end',
    flex: 1,
    marginEnd: 16,
  },
});
