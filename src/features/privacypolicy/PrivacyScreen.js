'use strict';
import {Container, Content, Body, Card,
  CardItem, Left, Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {FlatList} from 'react-native';
import styles from './styles';
import {HeaderBurger} from '../../components';
import PrivacyPolicy from '../../assets/privacy.json';

const PrivacyScreen = ({navigation}) =>
  <Container>
    <HeaderBurger title='Privacy Policy' navigation={navigation}/>
    <Content>
      <FlatList
        data={PrivacyPolicy}
        keyExtractor={(item) => item.title}
        renderItem = {({item}) =>
          <Card>
            <CardItem header>
              <Left>
                <Text style={styles.title}>{item.title}</Text>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Text style={styles.itemStyle}>{item.content}</Text>
              </Body>
            </CardItem>
          </Card>
        } />
    </Content>
  </Container>;

PrivacyScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default PrivacyScreen;
