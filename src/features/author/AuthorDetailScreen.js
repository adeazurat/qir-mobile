/* eslint-disable react/prop-types */
/* eslint-disable react-native/no-inline-styles */
'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import {View} from 'react-native';
import {Container, Content, Text} from 'native-base';
import {TextHeader} from '../../components';
import styles from './styles';
import AuthorDetailContainer from './AuthorDetailContainer';

class AuthorDetailScreen extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const {navigation} = this.props;
    const author = navigation.getParam('author', {});
    console.log(`AuthorDetailScreen:`);
    console.log(author);
    return (
      <Container>
        <Content>
          <TextHeader>Author</TextHeader>
          <View style = {styles.vertical}>
            <Text style = {styles.author}>{author.fullName}</Text>
            <Text style = {styles.itemStyle}>{author.organization}</Text>
            <Text style = {styles.itemStyle}>{author.country}</Text>
          </View>
          <TextHeader>Schedule</TextHeader>
          <AuthorDetailContainer navigation={navigation}
            authorName={author.fullName}/>
        </Content>
      </Container>
    );
  }
};

AuthorDetailScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default AuthorDetailScreen;
