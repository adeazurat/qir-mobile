'use strict';
import {StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

export default StyleSheet.create({
  header: {
    backgroundColor: qir.brandPrimary,
  },
  horizontal: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginVertical: 8,
  },
  vertical: {
    alignItems: 'flex-start',
    flexDirection: 'column',
    padding: 8,
  },
  author: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 8,
  },
  container: {
    alignItems: 'flex-start',
    flexDirection: 'column',
    padding: 8,
  },
  separator: {
    borderWidth: 1,
    borderColor: qir.brandSoft,
  },
  iconStyle: {
    fontSize: 14,
  },
  itemStyle: {
    color: qir.brandGrey,
    fontSize: 14,
  },
  input: {
    fontSize: 14,
  },
  search: {
    alignSelf: 'center',
  },
});
