'use strict';
import React from 'react';
import {Text, Icon} from 'native-base';
import {View} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const AuthorDetailItem = ({title, date, time, location}) =>
  <View style={styles.container}>
    <Text style={styles.title}>{title}</Text>
    <View style={styles.horizontal}>
      <Icon style={styles.iconStyle}
        type="SimpleLineIcons" name="clock" />
      <Text style={styles.itemStyle}>   {date}, {time}</Text>
    </View>
    <View style={styles.horizontal}>
      <Icon style={styles.iconStyle}
        type="SimpleLineIcons" name="location-pin" />
      <Text style={styles.itemStyle}>   {location}   </Text>
    </View>
    <View style={styles.separator}/>
  </View>; ;

AuthorDetailItem.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
};

export default AuthorDetailItem;
