// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';
import {Container, Content, Header, Spinner, Tab, Tabs,
  Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {TextHeader2} from '../../components';
import {getData} from '../../utils';
import createEventTabHeading from './createEventTabHeading';
import EventList from './EventList';
import styles from './styles';
const JULY = 6;
const DATES = [
  {day: new Date(Date.UTC(2019, JULY, 22)), title: 'Welcoming Guest'},
  {day: new Date(Date.UTC(2019, JULY, 23)), title: 'Conference'},
  {day: new Date(Date.UTC(2019, JULY, 24)), title: 'Conference'},
  {day: new Date(Date.UTC(2019, JULY, 25)), title: 'Social Tour'},
];

class EventListScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      loading: false,
      loaded: false,
      data: [],
    };
  }

  async componentDidMount() {
    this.setState({loading: true});
    try {
      const {endpoint} = this.props;
      const {data} = await getData(endpoint);
      console.log('EventListScreen#componentDidMount()',
          'Received data from:', endpoint, data);
      this.setState({
        loading: false,
        loaded: true,
        data: data,
      });
    } catch (error) {
      console.error('EventListScreen#componentDidMount()',
          'Error:', error);
      this.setState({
        hasError: true,
        loading: false,
        loaded: false,
      });
    }
  }

  render() {
    const {navigation} = this.props;
    if (this.state.hasError) {
      return this.renderError();
    }
    if (this.state.loading) {
      return this.renderLoading();
    } else {
      return (
        <Container>
          <Header hasTabs style={styles.header}>
            <TextHeader2 first="Event" second="Rundown" />
          </Header>
          <Content>
            <Tabs tabBarPosition="overlayTop">
              {
                DATES.map((date) => {
                  const {day, title} = date;
                  return (
                    <Tab key={day}
                      heading={createEventTabHeading(day, title)} >
                      <EventList navigation={navigation}
                        data={this.state.data.filter((event) =>
                          event.date.substring(0, 2) === String(day.getDate())
                        )} />
                    </Tab>
                  );
                })
              }
            </Tabs>
          </Content>
        </Container>
      );
    }
  }

  renderError() {
    return (
      <Container>
        <Content>
          <Text>
            Oops! There is a problem with the app or the network connection.
          </Text>
        </Content>
      </Container>
    );
  }

  renderLoading() {
    return (
      <Container>
        <Content>
          <Spinner />
        </Content>
      </Container>
    );
  }
}

EventListScreen.propTypes = {
  endpoint: PropTypes.string,
  navigation: PropTypes.object,
};

EventListScreen.defaultProps = {
  endpoint: 'https://asia-northeast1-qir-ftui.cloudfunctions.net/getEvents',
};

export default EventListScreen;
