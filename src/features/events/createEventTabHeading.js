// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';
import {TabHeading, Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import getDayShorthand from '../../utils/getDayShorthand';
import styles from './styles';

const createEventTabHeading = (date, title) =>
  <TabHeading style={styles.style}>
    <Text style={styles.lineHeight}>
      {getDayShorthand(date)} {date.getDate()} {'\n'}{title}
    </Text>
  </TabHeading>;

createEventTabHeading.propTypes = {
  date: PropTypes.objectOf(Date).isRequired,
  title: PropTypes.string.isRequired,
};

export default createEventTabHeading;
