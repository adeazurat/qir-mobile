'use strict';
import {StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

export default StyleSheet.create({
  header: {
    backgroundColor: qir.brandPrimary,
  },
  listItemContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 8,
  },
  listItemIcon: {
    fontSize: 16,
  },
  listItemLeftColumn: {
    flex: 3,
    flexDirection: 'column',
  },
  listItemRightColumn: {
    alignItems: 'flex-end',
    flex: 1,
    marginEnd: 16,
  },
  listItemTimePlaceText: {
    fontSize: 14,
    marginTop: 2,
  },
  listItemTitleText: {
    fontWeight: 'bold',
  },
  style: {
    backgroundColor: qir.brandLight,
  },
  lineHeight: {
    lineHeight: 10,
    fontSize: 10,
    textAlign: 'center',
  },
});
