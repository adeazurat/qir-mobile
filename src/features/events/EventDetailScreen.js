/* eslint-disable max-len */
/* eslint-disable react-native/no-inline-styles */
'use strict';
import NetInfo from '@react-native-community/netinfo';
import {Container, Content, Spinner, Text, Thumbnail} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import assets from '../../assets';
import {TextHeader} from '../../components';
import {getData} from '../../utils';
const NOT_FOUND = '';

class EventDetailScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.speakerName = props.navigation.getParam('speaker', NOT_FOUND);
    this.eventDate = props.navigation.getParam('date', NOT_FOUND);
    this.eventTime = props.navigation.getParam('time', NOT_FOUND);
    this.eventPlace = props.navigation.getParam('place', NOT_FOUND);

    this.state = {
      status: 'OFFLINE',
      data: [],
    };
  }

  async componentDidMount() {
    if (this.state.status !== 'SUCCESS') {
      const connected = await NetInfo.isConnected.fetch();

      if (connected) {
        this.setState({status: 'LOADING'});

        try {
          const speakerData = await getData(this.props.speakersEndpoint, {
            name: this.speakerName,
          });
          this.setState({
            status: 'SUCCESS',
            data: speakerData.data,
          });
        } catch (error) {
          console.error(error);
          this.setState({status: 'ERROR'});
        }
      }
    }
  }

  componentWillUnmount() {
    this.setState({data: []});
  }

  render() {
    if (this.state.status === 'LOADING') {
      return <Spinner />;
    } else if (this.state.status === 'SUCCESS') {
      return this.renderEventDetail();
    } else if (this.state.status === 'OFFLINE') {
      return <Text>You are offline!</Text>;
    } else {
      return <Text>An error has occurred!</Text>;
    }
  }

  renderEventDetail() {
    const {name, affiliation} = this.state.data[0];
    return (
      <Container>
        <Content>
          <TextHeader>Speaker</TextHeader>
          <View style={ {margin: 4} }>
            <Thumbnail source={assets.profile_placeholder} />
            <Text style={styles.text}>{name}</Text>
            <Text style={styles.text}>{affiliation}</Text>
            <Text style={styles.text}>{this.eventDate}, {this.eventTime}</Text>
          </View>
          <TextHeader>Description</TextHeader>
          <View style={ {margin: 4} }>
            <Text style={styles.text}> _ </Text>
          </View>
        </Content>
      </Container>
    );
  }
};

const styles = StyleSheet.create({
  text: {
    marginHorizontal: 4,
  },
});

EventDetailScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
  eventsEndpoint: PropTypes.string,
  speakersEndpoint: PropTypes.string,
};

EventDetailScreen.defaultProps = {
  eventsEndpoint: 'https://asia-northeast1-qir-ftui.cloudfunctions.net/getEvents',
  speakersEndpoint: 'https://asia-northeast1-qir-ftui.cloudfunctions.net/speakers',
};

EventDetailScreen.navigationOptions = {
  title: 'Event Details',
};

export default EventDetailScreen;
