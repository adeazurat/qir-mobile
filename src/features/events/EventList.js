'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import {FlatList} from 'react-native';
import EventListItem from './EventListItem';

const EventList = ({data, navigation}) =>
  <FlatList
    keyExtractor={(item, _) => String(item.id)}
    data={data}
    renderItem={(event) => createListItem(event, navigation)} />;

const createListItem = ({item}, navigation) => {
  if (item.isParallel === 'TRUE') {
    return (<EventListItem
      title={item.program}
      date={item.date}
      time={item.time}
      place={item.place}
      speaker={item.speaker}
      navigation={navigation}
      isParallel
    />);
  } else {
    return (<EventListItem
      title={item.program}
      date={item.date}
      time={item.time}
      place={item.place}
      speaker={item.speaker}
      navigation={navigation}
    />);
  }
};

EventList.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  navigation: PropTypes.object.isRequired,
};

export default EventList;
