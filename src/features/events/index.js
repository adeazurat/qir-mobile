'use strict';
import EventDetailScreen from './EventDetailScreen';
import EventListScreen from './EventListScreen';

export {EventListScreen, EventDetailScreen};
