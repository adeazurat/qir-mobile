'use strict';
import {Icon, Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {TouchableHighlight, View} from 'react-native';
import styles from './styles';
import qir from '../../../native-base-theme/variables/qir';

const EventListItem = (props) => createOnPressContainer(props,
    <View style={styles.listItemContainer}>
      <View style={styles.listItemLeftColumn}>
        <Text style={styles.listItemTitleText}>{props.title}</Text>
        {props.speaker !== '' &&
        <Text style={styles.listItemTitleText}>{props.speaker}</Text>}
        {!props.time.startsWith('TBA') &&
        <Text style={styles.listItemTimePlaceText}>
          {props.place ? `${props.time}, ${props.place}` : `${props.time}`}
        </Text>}
      </View>
      {(props.speaker !== '' || props.title.startsWith('Parallel')) &&
      <View style={styles.listItemRightColumn}>
        <Icon style={styles.listItemIcon} type="SimpleLineIcons"
          name='arrow-right' />
      </View>}
    </View>
);

const createOnPressContainer = (props, children) => {
  let onPressHandler = () => console.log('Clicked');

  if (props.speaker !== '') {
    onPressHandler = () => props.navigation.push('EventDetail', {
      speaker: props.speaker,
      date: props.date,
      time: props.time,
      place: props.place,
    });
  }

  if (props.isParallel) {
    onPressHandler = () => props.navigation.push('SymposiaList');
  }

  return (
    <TouchableHighlight underlayColor={qir.brandLighter}
      onPress={onPressHandler}>
      {children}
    </TouchableHighlight>
  );
};

createOnPressContainer.propTypes = {
  date: PropTypes.string.isRequired,
  isParallel: PropTypes.bool,
  navigation: PropTypes.object,
  place: PropTypes.string,
  speaker: PropTypes.string,
  time: PropTypes.string.isRequired,
};
;
EventListItem.propTypes = {
  date: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  navigation: PropTypes.object,
  speaker: PropTypes.string,
  time: PropTypes.string.isRequired,
};

export default EventListItem;
