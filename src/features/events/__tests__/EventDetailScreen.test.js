'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import EventDetailScreen from '../EventDetailScreen';

describe('An event detail screen component', () => {
  it('matches its snapshot correctly', () => {
    const mockNavigation = {
      push: jest.fn(),
      getParam: jest.fn((param, defaultValue) => {
        switch (param) {
          case 'speaker':
            return 'Giorno Giovanna';
            break;
          case 'date':
            return '22-07-2019';
            break;
          case 'time':
            return '10:00 - 13:00';
            break;
          default:
            return defaultValue;
            break;
        }
      }),
    };
    const component = TestRenderer.create(
        <EventDetailScreen navigation={mockNavigation} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
