'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import EventList from '../EventList';

describe('An event list component', () => {
  it('matches its snapshot correctly', () => {
    const mockNavigation = {
      push: jest.fn(),
    };
    const fakeEventData = [
      {
        id: 1,
        program: 'Event 1',
        date: '22-07-2019',
        time: '10:00 - 11:00',
        place: 'Test Room 1',
      },
      {
        id: 2,
        program: 'Event 2',
        date: '22-07-2019',
        time: '14:00 - 15:00',
        place: 'Test Room 2',
        isParallel: 'TRUE',
      },
      {
        id: 3,
        program: 'Event 3',
        date: '23-07-2019',
        time: '18:00 - 19:00',
        place: 'Test Room 3',
        speaker: 'Prof. Giorno Giovanna',
      },
    ];
    const eventListComponent = TestRenderer.create(
        <EventList eventData={fakeEventData} navigation={mockNavigation} />
    );

    expect(eventListComponent.toJSON()).toMatchSnapshot();
  });
});
