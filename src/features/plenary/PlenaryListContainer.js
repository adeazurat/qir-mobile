'use strict';
import NetInfo from '@react-native-community/netinfo';
import {Body, Button, Card, CardItem, Icon, Input, Item, Spinner,
  Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {FlatList, View} from 'react-native';
import {ConnectionStatus, getData, LoadStatus} from '../../utils';
import {ListItem, TextHeader} from './../../components';
import styles from './styles';

class PlenaryListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data ? props.data : [],
      query: '',
      connectionStatus: ConnectionStatus.OFFLINE,
      loadStatus: props.data ? LoadStatus.LOADED : LoadStatus.EMPTY,
    };
    this._resetState = this._resetState.bind(this);
    this._connectionChangeListener = this._connectionChangeListener.bind(this);
    this._loadData = this._loadData.bind(this);
    this._unloadData = this._unloadData.bind(this);
    this._createListItem = this._createListItem.bind(this);
  }
  componentDidMount() {
    console.log('PlenaryListContainer#componentDidMount():',
        'Add connection change listener');
    NetInfo.addEventListener('connectionChange',
        this._connectionChangeListener);
  }

  componentWillUnmount() {
    console.log('PlenaryListContainer#componentWillUnmount():',
        'Remove connection change listener');
    NetInfo.removeEventListener('connectionChange',
        this._connectionChangeListener);

    console.log('PlenaryListCOntainer#componentWillUnmount():',
        'Reset state');
    this._resetState();
  }

  render() {
    if (this.state.loadStatus === LoadStatus.LOADING) {
      return this._renderLoading();
    } else if (this.state.loadStatus === LoadStatus.LOADED) {
      return this._renderLoaded();
    } else if (this.state.loadStatus === LoadStatus.ERROR) {
      return this._renderError(this.props.errorMessage);
    } else { // (Empty data/offline)
      return this._renderError('You are currently offline');
    }
  }

  _renderLoading() {
    return <Spinner />;
  }

  _renderLoaded() {
    console.log('PlenaryListContainer#_renderLoaded():',
        '# of entries:', this.state.data.length);
    return (
      <View>
        <Item underline>
          <Input style={styles.input} placeholder="Search name of speaker"
            placeholderTextColor="gray"
            onChangeText={(text) => this.setState({query: text})} />
          <Button style={styles.search} transparent small>
            <Icon name="magnifier" type="SimpleLineIcons"/>
          </Button>
        </Item>
        <FlatList
          ListEmptyComponent={createEmptyListItemComponent(this.state.query)}
          ListHeaderComponent={<TextHeader>Keynote Speakers</TextHeader>}
          data={filterByName(filterByType(this.state.data, 'keynote'),
              this.state.query)}
          keyExtractor={(item, id) => String(`${item.fullName}${id}`)}
          renderItem={this._createListItem} />
        <FlatList
          ListEmptyComponent={createEmptyListItemComponent(this.state.query)}
          ListHeaderComponent={<TextHeader>Invited Speakers</TextHeader>}
          data={filterByName(filterByType(this.state.data, 'invited'),
              this.state.query)}
          keyExtractor={(item, id) => String(`${item.fullName}${id}`)}
          renderItem={this._createListItem} />
      </View>
    );
  }

  _createListItem({item, index}) {
    const {navigation} = this.props;

    return (
      <ListItem onPress={() => navigation.push('PlenaryDetailScreen',
          {speaker: item})}>
        <View style={styles.listItem}>
          <View style={styles.indexColumn}>
            <Text style={styles.index}>{String(index + 1)}</Text>
          </View>
          <View style={styles.contentColumn}>
            <Text style={styles.name}>{item.name}</Text>
            <Text style={styles.itemStyle}>{item.affiliation}</Text>
            <Text style={styles.itemStyle}>{item.country}</Text>
          </View>
        </View>
      </ListItem>
    );
  }

  _renderError(message) {
    return (
      <Card>
        <CardItem>
          <Body>
            {console.log('PlenaryListContainer#_renderError():',
                'Rendering error message', message)}
            <Text>{message}</Text>
          </Body>
        </CardItem>
      </Card>
    );
  }

  _resetState() {
    this.setState({
      data: [],
      query: '',
      connectionStatus: ConnectionStatus.OFFLINE,
      loadStatus: LoadStatus.EMPTY,
    });
  }

  _connectionChangeListener(connectionInfo) {
    switch (connectionInfo.type) {
      case 'none':
      case 'unknown':
        if (this.state.connectionStatus === ConnectionStatus.ONLINE) {
          console.log('PlenaryListContainer#_connectionChangeListener():',
              'Connection switched from online to offline');
          this.setState({connectionStatus: ConnectionStatus.OFFLINE});
          this._unloadData();
        }
        break;
      case 'wifi':
      case 'cellular':
        if (this.state.connectionStatus === ConnectionStatus.OFFLINE) {
          console.log('PlenaryListContainer#_connectionChangeListener():',
              'Connection switched from offline to online');
          this.setState({connectionStatus: ConnectionStatus.ONLINE});
          this._loadData();
        }
        break;
      default:
        console.log('PlenaryListContainer#_connectionChangeListener():',
            'Unrecognised connection type', connectionInfo.type);
        break;
    }
  }
  _loadData() {
    const {endpoint} = this.props;
    switch (this.state.loadStatus) {
      case LoadStatus.LOADED: // TODO: Reload data from cache instead of API
      case LoadStatus.EMPTY:
        console.log('PlenaryListContainer#_loadData():',
            'Loading data from:', endpoint);
        this.setState({loadStatus: LoadStatus.LOADING});

        getData(endpoint).then((json) => {
          console.log('PlenaryListContainer#_loadData():',
              'Received data in JSON format', json);
          this.setState({
            data: json.data,
            loadStatus: LoadStatus.LOADED,
          });
        }).catch((error) => {
          console.error('PlenaryListContainer#_loadData():',
              'There was a problem communicating with the API at:',
              endpoint);
          console.error(error);
          this.setState({loadStatus: LoadStatus.ERROR});
        });
        break;
      case LoadStatus.LOADING:
      case LoadStatus.ERROR:
        break; // Do nothing
      default:
        break; // Do nothing
    }
  }

  _unloadData() {
    switch (this.state.loadStatus) {
      // TODO: Store data into cache instead of clearing it
      case LoadStatus.LOADED:
        console.log('PlenaryListContainer#_unloadData():',
            'Unload data');
        this.setState({
          data: [],
          loadStatus: LoadStatus.EMPTY,
        });
        break;
      case LoadStatus.EMPTY:
      case LoadStatus.LOADING:
      case LoadStatus.ERROR:
        break; // Do nothing
      default:
        break; // Do nothing
    }
  }
}

const filterByName = (data, name) => {
  if (name === '') return data;
  const filteredData = data.filter((entry) =>
    entry.name.toLowerCase().includes(name.toLowerCase()));
  return filteredData;
};

const filterByType = (data, type) => {
  if (type === '') return data;
  return data.filter((entry) => entry.type === type);
};

const createEmptyListItemComponent = (query) =>
  <Text>No match found for {`'${query}'`}</Text>;


PlenaryListContainer.propTypes = {
  endpoint: PropTypes.string,
  errorMessage: PropTypes.string,
  data: PropTypes.arrayOf(PropTypes.object),
  navigation: PropTypes.object.isRequired,
};

PlenaryListContainer.defaultProps = {
  errorMessage: 'There is a problem with the app or the Internet connection',
  endpoint: 'https://asia-northeast1-qir-ftui.cloudfunctions.net/speakers',
};

export default PlenaryListContainer;
