'use strict';
import {StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

export default StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    flexDirection: 'column',
    padding: 8,
  },
  contentColumn: {
    flex: 3,
    flexDirection: 'column',
  },
  detailStyle: {
    color: qir.brandGrey,
    fontSize: 14,
    padding: 8,
  },
  header: {
    backgroundColor: qir.brandPrimary,
  },
  horizontal: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginVertical: 4,
  },
  iconStyle: {
    fontSize: 14,
  },
  index: {
    fontWeight: 'bold',
  },
  indexColumn: {
    alignSelf: 'center',
    flex: 1,
  },
  input: {
    fontSize: 14,
  },
  itemStyle: {
    fontStyle: 'italic',
    color: qir.brandGrey,
    fontSize: 14,
  },
  listItem: {
    flexDirection: 'row',
    marginBottom: 8,
  },
  name: {
    fontWeight: 'bold',
  },
  photoIcon: {
    fontSize: 64,
  },
  search: {
    alignSelf: 'center',
  },
  sessionDetail: {
    color: qir.brandGrey,
    fontSize: 12,
  },
  speakerContainer: {
    flexDirection: 'column',
    marginLeft: 8,
  },
  text: {
    marginVertical: 16,
  },
});
