'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import PlenaryListContainer from '../PlenaryListContainer';

describe('A plenary session list container component', () => {
  const sampleData = [
    {
      name: 'Jonathan Joestar',
      affiliation: 'Speedwagon Foundation',
      country: 'United States of America',
      type: 'keynote',
    },
    {
      name: 'Dio Brando',
      affiliation: 'The World',
      country: 'United States of America',
      type: 'keynote',
    },
    {
      name: 'Joseph Joestar',
      affiliation: 'Hermit Purple',
      country: 'United States of America',
      type: 'invited',
    },
    {
      name: 'Giorno Giovanna',
      affiliation: 'Gang Star',
      country: 'Italy',
      type: 'invited',
    },
    {
      name: 'Bruno Buccelati',
      affiliation: 'Passione',
      country: 'Italy',
      type: 'keynote',
    },
    {
      name: 'Iggy',
      affiliation: 'The Fool',
      country: 'Canada',
      type: 'invited',
    },
  ];
  let mockNavigation = {};

  beforeEach(() => {
    mockNavigation = {push: jest.fn()};
  });

  it('matches with the snapshot', () => {
    const component = TestRenderer.create(
        <PlenaryListContainer data={sampleData} navigation={mockNavigation} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
