'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import MessageInput from '../MessageInput';

describe('A message input component', () => {
  let mockSubmitAction = {};

  beforeEach(() => {
    mockSubmitAction = jest.fn();
  });

  it('matches with the snapshot', () => {
    const component = TestRenderer.create(
        <MessageInput submitAction={mockSubmitAction} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });

  // TODO Add test case(s) for simulating user input handling
});

