/* eslint-disable react-native/no-inline-styles */
'use strict';
import PropTypes from 'prop-types';
import React from 'react';
import {TextInput, TouchableOpacity} from 'react-native';
import {View, KeyboardAvoidingView} from 'react-native';
import {Icon} from 'react-native-elements';
import qir from '../../../native-base-theme/variables/qir';
import styles from './styles';

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {currentChat: ''};
    this.changeAction = this.changeAction.bind(this);
    this.submitChat = this.submitChat.bind(this);
  }

  changeAction(text) {
    this.setState({currentChat: text});
  }

  submitChat() {
    if (this.state.currentChat.trim() === '') return;
    this.props.submitAction(this.state.currentChat.trim());
    this.setState({currentChat: ''});
  }

  render() {
    return (
      <KeyboardAvoidingView 
        behavior="padding">
        <TextInput style={styles.TextInput}
          height={60}
          selectionColor="#d879d888"
          value={this.state.currentChat}
          onChangeText={this.changeAction}
          onSubmitEditing={this.submitChat}
          placeholder='Write a message...' />
        <TouchableOpacity
          style={styles.submitButton}
          onPress={this.submitChat}>
          <Icon name='send' size={30} color={qir.brandPrimary} />
        </TouchableOpacity>
        <View style={{height: 60}} />
      </KeyboardAvoidingView>
    );
  }
}

MessageInput.propTypes = {
  submitAction: PropTypes.func.isRequired,
};

export default MessageInput;
