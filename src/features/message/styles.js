'use strict';
import {StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: 'column',
  },
  messageListContainer: {
    flex: 1,
    display: 'flex',
  },
  inputContainer: {
    display: 'flex',
    flexDirection: 'row',
    borderTopColor: 'lightgray',
    borderTopWidth: 1,
    paddingLeft: 4,
    paddingRight: 4,
  },
  textInput: {
    flexGrow: 0,
    flexBasis: '90%',
    paddingLeft: 8,
    paddingRight: 8,
    fontSize: 18,
  },
  submitButton: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // width:45,
    // height:45,
  },
  adminBubble: {
    backgroundColor: qir.brandLight,
  },
  // eslint-disable-next-line react-native/no-color-literals
  userBubble: {
    backgroundColor: '#b3a4d5',
  },
  bubble: {
    borderRadius: 10,
    paddingBottom: 8,
    paddingHorizontal: 14,
    paddingTop: 8,
    minWidth: 0,
    maxWidth: '70%',
  },
  adminBubbleContainer: {
    flexDirection: 'row',
  },
  userBubbleContainer: {
    flexDirection: 'row-reverse',
  },
  bubbleContainer: {
    width: '100%',
    display: 'flex',
    marginBottom: 6,
    marginTop: 6,
    paddingLeft: 12,
    paddingRight: 12,
  },
  bubbleContent: {
    fontSize: 18,
  },
  timestamp: {
    alignSelf: 'flex-end',
    marginLeft: 8,
    marginRight: 8,
    color: '#aaaaaa',
  },
  noChats: {
    textAlign: 'center',
    paddingTop: 32,
    fontSize: 20,
  },
  addPadding: {
    height: 60,
  },
});
