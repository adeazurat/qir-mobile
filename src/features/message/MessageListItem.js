'use strict';
import {Text} from 'native-base';
import React from 'react';
import {View} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';

class MessageListItem extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  formatDate = (date) => {
    date = new Date(date.toDate());
    const month =
      ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][date.getMonth()];
    const day = date.getDate();
    let minute = date.getMinutes();
    minute = (minute < 10) ? ('0' + minute) : minute;
    let hour = date.getHours();
    hour = (hour < 10) ? ('0' + hour) : hour;
    console.log(`${date.getDay()} ${month} ${hour}:${minute}`);
    return `${month} ${day} ${hour}:${minute}`;
  }


  render() {
    return (
      <View style={this.props.admin ?
      [styles.bubbleContainer, styles.adminBubbleContainer] :
      [styles.bubbleContainer, styles.userBubbleContainer]}>
        <View style={this.props.admin ?
        [styles.bubble, styles.adminBubble] :
        [styles.bubble, styles.userBubble]}>
          <Text style={styles.bubbleContent}>{this.props.content}</Text>
        </View>
        <Text style={styles.timestamp}>
          {this.formatDate(this.props.timestamp)}
        </Text>
      </View>
    );
  }
}

MessageListItem.propTypes = {
  admin: PropTypes.any, // TODO Define the actual required type
  content: PropTypes.string.isRequired,
  timestamp: PropTypes.any, // TODO Define the actual required type
};

export default MessageListItem;
