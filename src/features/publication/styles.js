'use strict';
import {StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

export default StyleSheet.create({
  header: {
    backgroundColor: qir.brandPrimary,
    flexDirection: 'row',
  },
  headerTitle: {
    color: 'white',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    padding: 12,
    alignItems: 'center',
  },
  burger: {
    color: qir.iconColor,
    fontSize: 14,
    alignItems: 'flex-start',
  },
  itemStyle: {
    color: qir.brandGrey,
    fontSize: 14,
    padding: 12,
  },
  div: {
    flex: 1,
  },
});

