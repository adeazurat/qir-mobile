'use strict';
import React from 'react';
import {TouchableHighlight, View} from 'react-native';
import {Text} from 'native-base';
import PropTypes from 'prop-types';
import styles from './styles';
import qir from '../../../native-base-theme/variables/qir';

const SubmissionListItem = ({title, authors, index, onPress}) =>
  <View style={styles.listItem}>
    <View style={styles.indexColumn}>
      <Text style={styles.index}>{String(index + 1)}</Text>
    </View>
    <View style={styles.contentColumn}>
      <Text style={styles.titleList}>{title}</Text>
      <Text style={styles.authorsList}>{authors}</Text>
      <TouchableHighlight underlayColor={qir.brandLighter}
        onPress={onPress}>
        <Text style={styles.viewDetail}>View Detail</Text>
      </TouchableHighlight>
    </View>
  </View>;

SubmissionListItem.propTypes = {
  title: PropTypes.string.isRequired,
  authors: PropTypes.string.isRequired,
  organization: PropTypes.string,
  index: PropTypes.number,
  onPress: PropTypes.func.isRequired,
};

export default SubmissionListItem;
