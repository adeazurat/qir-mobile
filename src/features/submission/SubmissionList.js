'use strict';
import PropTypes from 'prop-types';
import React from 'react';
import {SectionList, View} from 'react-native';
import {TextHeader} from '../../components';
import styles from './styles';
import SubmissionListItem from './SubmissionListItem';
import qir from '../../../native-base-theme/variables/qir';

const SubmissionList = ({data, navigation}) =>
  <SectionList
    keyExtractor={(_, index) => String(index)}
    sections={createSections(data)}
    ItemSeparatorComponent={() => <View style={styles.separator}></View>}
    renderSectionHeader={({section}) =>
      <TextHeader
        textColor={qir.brandDark}
        backgroundColor={qir.brandSoft}>{section.title}</TextHeader>}
    renderItem={({item, index, _}) =>
      <SubmissionListItem
        title={item.title}
        authors={item.authors}
        index={index}
        onPress={() => navigation.push(
            'SubmissionDetailScreen',
            {submission: createSubmission(item)})}
      />}
  />;

SubmissionList.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  navigation: PropTypes.object.isRequired,
};

const createSubmission = (item) => {
  const submissionObject = {
    title: item.title,
    date: item.date,
    time: item.time,
    location: item.location,
    chair: item.chair,
    authors: item.authors,
    abstract: item.abstract,
  };

  return submissionObject;
};

const createSections = (data) => {
  const sections = [];
  const map = new Map();

  data.forEach((entry) => {
    const uniqueTime = `${entry.date}`;
    const bucket = map.get(uniqueTime);

    if (!bucket) {
      map.set(uniqueTime, [entry]);
    } else {
      bucket.push(entry);
    }
  });

  map.forEach((_, key, map) => {
    sections.push({
      title: key,
      data: map.get(key),
    });
  });

  return sections;
};


export default SubmissionList;
