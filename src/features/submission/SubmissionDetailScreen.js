/* eslint-disable react/prop-types */
'use strict';
import React from 'react';
import {View} from 'react-native';
import {Container, Content, Icon, Text} from 'native-base';
import styles from './styles';

const SubmissionDetailScreen = ({navigation}) => {
  const {title, date, time, location, chair, authors, abstract} =
    navigation.getParam('submission', {});

  return <Container>
    <Content padder>
      <Text style={styles.title}>{title}</Text>
      <View style={styles.horizontal}>
        <Icon style={styles.iconStyle}
          type="SimpleLineIcons" name="clock" />
        <Text style={styles.itemStyle}>   {date}, {time}</Text>
      </View>
      <View style={styles.horizontal}>
        <Icon style={styles.iconStyle}
          type="SimpleLineIcons" name="location-pin" />
        <Text style={styles.itemStyle}>   {location}   </Text>
        <Icon style={styles.iconStyle}
          type="SimpleLineIcons" name="graduation" />
        <Text style={styles.itemStyle}>   {chair}</Text>
      </View>
      <View style={styles.horizontal}>
        <Icon style={styles.iconStyle}
          type="SimpleLineIcons" name="people" />
        <Text style={styles.itemStyle}>   {authors}</Text>
      </View>
      <Text style={styles.detailAbstractHeadingText}>Abstract</Text>
      <Text style={styles.itemStyle}>{abstract}</Text>
    </Content>
  </Container>;
};

export default SubmissionDetailScreen;
