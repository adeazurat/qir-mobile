'use strict';
import SubmissionDetailScreen from './SubmissionDetailScreen';
import SubmissionListScreen from './SubmissionListScreen';

export {SubmissionListScreen, SubmissionDetailScreen};

