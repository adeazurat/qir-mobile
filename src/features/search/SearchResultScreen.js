'use strict';
import React from 'react';
import {Button, Container, Input, Item, Icon, Content} from 'native-base';
import {TextHeader, TextHeader2} from '../../components';
import SearchResultList from './SearchResultList';
import PropTypes from 'prop-types';
import styles from './styles';
const EMPTY_TEXT = '';

class SearchResultScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nextQuery: '',
    };
  }

  render() {
    const {navigation} = this.props;
    const query = navigation.getParam('query', EMPTY_TEXT);

    return (
      <Container>
        <TextHeader2 first="Search" second="Results" />
        <Content>
          <Item underline>
            <Input style={styles.input}
              placeholder="Search by author, paper, keywords, or room"
              placeholderTextColor="gray"
              onChangeText={(text) => this.setState({nextQuery: text})}
              onSubmitEditing={() => {
                navigation.push('SearchResultScreen',
                    {query: this.state.nextQuery});
              }} />
            <Button style={styles.search} transparent small onPress={() => {
              navigation.push('SearchResultScreen',
                  {query: this.state.nextQuery});
            }}>
              <Icon name="magnifier" type="SimpleLineIcons"/>
            </Button>
          </Item>
          <TextHeader>By author</TextHeader>
          <SearchResultList dataId="fullName" navigation={navigation}
            query={query} type="author" style={styles.space} />
          <TextHeader style={styles.space}>By title</TextHeader>
          <SearchResultList dataId="id" navigation={navigation}
            query={query} type="title" style={styles.space} />
          <TextHeader style={styles.space}>By keyword</TextHeader>
          <SearchResultList dataId="id" navigation={navigation}
            query={query} type="keyword" style={styles.space} />
          <TextHeader style={styles.space}>By room</TextHeader>
          <SearchResultList dataId="id" navigation={navigation}
            query={query} type="room" style={styles.space} />
        </Content>
      </Container>
    );
  }
}

SearchResultScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default SearchResultScreen;
