'use strict';
import React from 'react';
// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import {Icon} from 'native-base';
import {shallow} from 'enzyme';
import RoundedRectangleButton from '../RoundedRectangleButton';

describe('A rounded rectangle button', () => {
  it('matches its snapshot correctly', () => {
    const buttonComponent = renderer.create(
        <RoundedRectangleButton route="Test Screen"
          text="Test">
          <Icon type="SimpleLineIcons"
            name='arrow-right' />
        </RoundedRectangleButton>
    );

    expect(buttonComponent.toJSON()).toMatchSnapshot();
  });
  it('can be clicked', () => {
    const mockNavigation = {
      push: jest.fn(),
    };
    const wrapper = shallow(
        <RoundedRectangleButton route="Test Screen" text="Test"
          navigation={mockNavigation}>
          <Icon type="SimpleLineIcons"
            name='arrow-right' />
        </RoundedRectangleButton>
    );
    const anotherWrapper = shallow(
        <RoundedRectangleButton route="Test Screen" text="Test">
          <Icon type="SimpleLineIcons"
            name='arrow-right' />
        </RoundedRectangleButton>
    );

    wrapper.simulate('press');
    anotherWrapper.simulate('press');

    expect(mockNavigation.push).toBeCalled();
  });
});
