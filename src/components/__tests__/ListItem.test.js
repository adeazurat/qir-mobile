'use strict';
import React from 'react';
import {TouchableHighlight, View} from 'react-native';
import {Icon, Text} from 'native-base';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import {shallow} from 'enzyme';
import ListItem from '../ListItem';

describe('A list item component', () => {
  let mockNavigation = {};

  beforeEach(() => {
    mockNavigation = {push: jest.fn()};
  });

  // TODO Wrap text components in a View hierarchy, and fix the component
  it('matches with the snapshot', () => {
    const component = TestRenderer.create(
        <ListItem onPress={mockNavigation.push}>
          <Text>Foo</Text>
          <Text>Bar</Text>
        </ListItem>
    );

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('contains a pressable icon', () => {
    const wrapper = shallow(
        <ListItem onPress={mockNavigation.push}>
          <Text>Foo</Text>
          <Text>Bar</Text>
        </ListItem>
    );

    const pressableComponent = wrapper.find(TouchableHighlight);

    expect(pressableComponent.length).toEqual(2);
    expect(pressableComponent.at(0).children(View).length).toEqual(1);
    expect(pressableComponent.at(1).children(Icon).length).toEqual(1);
  });

  it('pressable', () => {
    const wrapper = shallow(
        <ListItem onPress={mockNavigation.push}>
          <Text>Foo</Text>
          <Text>Bar</Text>
        </ListItem>
    );

    wrapper.find(TouchableHighlight).at(0).simulate('press');
    expect(mockNavigation.push).toBeCalled();
    wrapper.find(TouchableHighlight).at(1).simulate('press');
    expect(mockNavigation.push).toBeCalled();
  });
});
