'use strict';
import PropTypes from 'prop-types';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Button, Icon, Text} from 'native-base';
import qir from '../../native-base-theme/variables/qir';

const HeaderBurger = ({title, navigation}) =>
  <View style={styles.header}>
    <View style={styles.divBurger}>
      <Button transparent onPress={() => navigation.toggleDrawer()}>
        <Icon name="menu" type="SimpleLineIcons"
          style={styles.burger} />
      </Button>
    </View>
    <View style={styles.divTitle}>
      <Text style={styles.headerTitle}>{title}</Text>
    </View>
  </View>;

const styles = StyleSheet.create({
  burger: {
    alignItems: 'flex-start',
    color: qir.iconColor,
    fontSize: 14,
  },
  divBurger: {
    flex: 1,
  },
  divTitle: {
    flex: 2,
  },
  header: {
    backgroundColor: qir.brandPrimary,
    flexDirection: 'row',
  },
  headerTitle: {
    alignItems: 'center',
    color: qir.brandLighter,
    fontWeight: 'bold',
    padding: 12,
    textTransform: 'uppercase',
  },
});

HeaderBurger.propTypes = {
  navigation: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
};

export default HeaderBurger;
