'use strict';
import PropTypes from 'prop-types';
import React from 'react';
import {Dimensions, Image, StyleSheet} from 'react-native';

const HeaderImage = ({source}) =>
  <Image
    resizeMode='cover'
    source={source}
    style={isPortrait() ? styles.portraitLogo : styles.landscapeLogo} />;

const isPortrait = () => {
  const {width, height} = Dimensions.get('window');
  return height > width;
};

HeaderImage.propTypes = {
  source: PropTypes.oneOfType([
    PropTypes.object, // Data type when running unit testing
    PropTypes.number, // Data type when running on device
  ]).isRequired,
};

const styles = StyleSheet.create({
  landscapeLogo: {
    height: null,
    width: 100,
  },
  portraitLogo: {
    height: 200,
    width: null,
  },
});

export default HeaderImage;
