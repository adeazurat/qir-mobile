// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';
import {Spinner, Thumbnail} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {Image} from 'react-native';
import {storage} from '../firebase';
import assets from '../assets';

const withFirebaseImage = (ImageComponent, storageRef) =>
  class FirebaseImage extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        hasError: false,
        loading: false,
        loaded: false,
        uri: '',
      };
    }

    componentDidMount() {
      this.setState({loading: true});
      storage.ref(storageRef).getDownloadURL().then((downloadURL) => {
        console.log('FirebaseImage#componentDidMount()',
            'Received download URL:', downloadURL);
        this.setState({
          loading: false,
          loaded: true,
          uri: downloadURL,
        });
      }).catch((error) => {
        console.error('FirebaseImage#componentDidMount()', 'Error:', error);
        this.setState({hasError: true, loading: false});
      });
    }

    render() {
      const {uri} = this.state;
      if (this.state.hasError) {
        return <ImageComponent {...this.props}
          source={assets.profile_placeholder} />;
      }

      if (this.state.loading || uri === '') {
        return <Spinner />;
      } else {
        return <ImageComponent {...this.props}
          source={{uri: uri}} />;
      }
    }
  };

withFirebaseImage.propTypes = {
  ImageComponent: PropTypes.oneOfType([Image, Thumbnail]).isRequired,
  storageRef: PropTypes.string.isRequired,
};

export default withFirebaseImage;
