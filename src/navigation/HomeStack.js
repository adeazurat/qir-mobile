'use strict';
import {Button, Icon} from 'native-base';
import React from 'react';
import {createStackNavigator} from 'react-navigation';
import {EventDetailScreen, EventListScreen} from '../features/events';
import {MainMenuScreen} from '../features/mainmenu';
import {NewsDetailScreen} from '../features/news';
import {PlenaryDetailScreen, PlenaryListScreen} from '../features/plenary';
import {SearchResultScreen} from '../features/search';
import {SubmissionDetailScreen,
  SubmissionListScreen} from '../features/submission';
import {SymposiaListScreen} from '../features/symposia';
import styles from './styles';

const routeConfig = {
  'MainMenuScreen': {
    screen: MainMenuScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Quality in Research',
      headerLeft: (
        <Button transparent onPress={() => navigation.toggleDrawer()}>
          <Icon name="md-menu" type="Ionicons" />
        </Button>
      ),
    }),
  },
  'NewsDetailScreen': {
    screen: NewsDetailScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'News',
    }),
  },
  'EventRundown': {
    screen: EventListScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Event Rundown',
    }),
  },
  'EventDetail': {
    screen: EventDetailScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Event Details',
    }),
  },
  'SymposiaList': {
    screen: SymposiaListScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Parallel Sessions',
    }),
  },
  'SubmissionList': {
    screen: SubmissionListScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Submitted Papers',
    }),
  },
  'SearchResultScreen': {
    screen: SearchResultScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Search Results',
    }),
  },
  'SubmissionDetailScreen': {
    screen: SubmissionDetailScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Paper Detail',
    }),
  },
  'PlenaryListScreen': {
    screen: PlenaryListScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Keynote/Invited Speakers',
    }),
  },
  'PlenaryDetailScreen': {
    screen: PlenaryDetailScreen,
    navigationOptions: ({navigation}) => ({
      headerStyle: styles.navbarHeader,
      title: 'Session Details',
    }),
  },

};

const stackNavigatorConfig = {
  initialRouteName: 'MainMenuScreen',
};

const HomeStack = createStackNavigator(routeConfig, stackNavigatorConfig);

export default HomeStack;
