/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
'use strict';
import {Icon} from 'native-base';
import React from 'react';
import {
  createBottomTabNavigator,
  StackActions,
} from 'react-navigation';
import qir from '../../native-base-theme/variables/qir';
import HomeDrawerNavigator from './HomeDrawerNavigator';
import MapStack from './MapStack';
import MessageStack from './MessageStack';
import styles from './styles';

const routeConfig = {
  'Home': HomeDrawerNavigator,
  'Venue Map': MapStack,
  'Chat': MessageStack,
};

const BottomTabNavigator = createBottomTabNavigator(routeConfig, {
  initialRouteName: 'Home',
  defaultNavigationOptions: ({navigation}) => ({
    tabBarIcon: ({focused, horizontal, tintColor}) => {
      const {routeName} = navigation.state;
      const iconType = 'FontAwesome';
      let iconName;

      switch (routeName) {
        case 'Home':
          iconName = 'home';
          break;
        case 'Venue Map':
          iconName = 'map-marker';
          break;
        case 'Chat':
          iconName = 'comments-o';
          break;
        default:
          console.warn(`BottomTabNavigator: Unknown route '${routeName}'`);
          iconName = 'home';
          break;
      }


      return <Icon type={iconType} name={iconName} color={tintColor}
        style={focused ? styles.bottomNavbarActiveTabIcon :
          styles.bottomNavbarTabIcon} />;
    },

    tabBarOnPress: ({navigation, defaultHandler}) => {
      const {routeName} = navigation.state;
      if (routeName === 'Home' && navigation.isFocused()) {
        console.log(`Subsequent focus on ${routeName}, resetting stack`);
        return navigation
            .dispatch(StackActions.reset({
              index: 0,
              key: 'MainMenuScreen',
              actions: [
                navigation.navigate({routeName: 'MainMenuScreen'}),
              ],
            }));
      }
      defaultHandler();
    },
  }),
  tabBarOptions: {
    activeTintColor: qir.brandPrimary,
    activeBackgroundColor: qir.footerDefaultBg,
    inactiveTintColor: '#717171',
    inactiveBackgroundColor: qir.footerDefaultBg,
  },
});

export default BottomTabNavigator;
