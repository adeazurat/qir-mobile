import {StyleSheet} from 'react-native';
import qir from '../../native-base-theme/variables/qir';

export default StyleSheet.create({
  bottomNavbarActiveTabIcon: {
    color: qir.brandPrimary,
    fontSize: 32,
  },
  bottomNavbarTabIcon: {
    fontSize: 32,
  },
  drawerLabelStyle: {
    color: 'white',
  },
  drawerIcon: {
    color: 'white',
  },
  navbarHeader: {
    backgroundColor: qir.toolbarDefaultBg,
  },
});
