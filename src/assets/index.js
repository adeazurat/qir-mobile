'use strict';

const assets = {
  profile_placeholder: require('./profile_placeholder.png'),
  header_logo: require('./header_logo_v2.jpg'),
};

export default assets;
