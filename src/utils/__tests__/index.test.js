'use strict';
import {getData} from '../';

describe('Utility functions', () => {
  describe('getData()', () => {
    beforeEach(() => {
      fetch.resetMocks();
    });

    it('resolves properly', async () => {
      fetch.mockResponse(JSON.stringify({
        data: 'test',
      }));

      const response = await getData('https://example.com');

      expect(response.data).toEqual('test');
      expect(fetch).toBeCalled();
    });

    it('handles query properly', async () => {
      fetch.mockResponse(JSON.stringify({
        data: 'test',
      }));

      await getData('https://example.com', {foo: 'bar', a: 'b'});

      expect(fetch.mock.calls[0][0]).toEqual('https://example.com?foo=bar&a=b&');
    });

    it('handles failed request', async () => {
      fetch.mockReject(new Error('A fake error message'));
      try {
        await getData('https://example.com', {foo: 'bar'});
      } catch (error) {
        expect(error.message.includes('https://example.com')).toBe(true);
      }
    });
  });
});
