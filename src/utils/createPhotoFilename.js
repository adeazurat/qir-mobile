// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';

export default (name, fileType) => {
  return name
      .replace(/\s{2,}/g, ' ')
      .trim()
      .replace(/[\,\.\(\)]/g, '')
      .replace(/\s{1}/g, '_')
      .toLowerCase() + `.${fileType.toLowerCase()}`;
};
