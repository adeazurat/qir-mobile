'use strict';
import {NativeModules} from 'react-native';

global.fetch = require('jest-fetch-mock');
// Workaround for undefined component error from react-native-gesture-handler
// v1.1.0 when running test suite
// Source: https://github.com/kmagiera/react-native-gesture-handler/issues/344#issuecomment-473529975
jest.mock('react-native-gesture-handler', () => {
  const View = require('react-native/Libraries/Components/View/View');
  return {
    Swipeable: View,
    DrawerLayout: View,
    State: {},
    ScrollView: View,
    Slider: View,
    Switch: View,
    TextInput: View,
    ToolbarAndroid: View,
    ViewPagerAndroid: View,
    DrawerLayoutAndroid: View,
    WebView: View,
    NativeViewGestureHandler: View,
    TapGestureHandler: View,
    FlingGestureHandler: View,
    ForceTouchGestureHandler: View,
    LongPressGestureHandler: View,
    PanGestureHandler: View,
    PinchGestureHandler: View,
    RotationGestureHandler: View,
    /* Buttons */
    RawButton: View,
    BaseButton: View,
    RectButton: View,
    BorderlessButton: View,
    /* Other */
    FlatList: View,
    gestureHandlerRootHOC: jest.fn(),
    Directions: {},
  };
});

// Additional workaround to handle drawer menu navigator
// Source: https://github.com/kmagiera/react-native-gesture-handler/issues/344#issuecomment-480333304
jest.mock('NativeModules', () => ({
  UIManager: {
    RCTView: () => {},
  },
  RNGestureHandlerModule: {
    attachGestureHandler: jest.fn(),
    createGestureHandler: jest.fn(),
    dropGestureHandler: jest.fn(),
    updateGestureHandler: jest.fn(),
    State: {},
    Directions: {},
  },
  PlatformConstants: {
    forceTouchAvailable: false,
  },
  KeyboardObserver: {
    addListener: jest.fn(),
    getConstants: jest.fn(),
    removeListeners: jest.fn(),
  },
}));

// As instructed in @react-native-community/netinfo README
NativeModules.RNCNetInfo = {
  getCurrentConnectivity: jest.fn(() => new Promise((resolve, reject) => {
  })),
  isConnectionMetered: jest.fn(),
  addListener: jest.fn(),
  removeListeners: jest.fn(),
};

// Mock react-native-device-info
NativeModules.RNDeviceInfo = {
  getUniqueID: jest.fn(() => 42),
};
