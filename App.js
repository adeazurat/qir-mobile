/* eslint-disable react/display-name */
'use strict';
import {createAppContainer} from 'react-navigation';
import {BottomTabNavigator} from './src/navigation';
import './src/init';

export default createAppContainer(BottomTabNavigator);
