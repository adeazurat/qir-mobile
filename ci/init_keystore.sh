#!/usr/bin/env bash
keytool -genkeypair -alias $REVIEW_ALIAS -keyalg RSA -keysize 4096 \
    -dname "$REVIEW_DNAME" -storepass $KEYSTORE_PASS \
    -keystore ~/qir.keystore
keytool -genkeypair -alias $RELEASE_ALIAS -keyalg RSA -keysize 4096 \
    -dname "$RELEASE_DNAME" -storepass $KEYSTORE_PASS \
    -keystore ~/qir.keystore
