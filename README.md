# QIR Mobile

[![pipeline status](https://gitlab.com/konfui/qir-mobile/badges/master/pipeline.svg)](https://gitlab.com/konfui/qir-mobile/commits/master)
[![coverage report](https://gitlab.com/konfui/qir-mobile/badges/master/coverage.svg)](https://gitlab.com/konfui/qir-mobile/commits/master)

## Prerequisites

- Node.js v8.15.1
  - Install Node.js using `nvm` (Node Version Manager):
  [Windows](https://github.com/coreybutler/nvm-windows),
  [Mac/Linux](https://github.com/creationix/nvm)
- `yarn` package manager
  - Install `yarn` using built-in `npm` package manager provided
  in base Node.js installation: `npm install -g yarn`
- Java v11
- Android SDK

## Build

For the first time (or whenever someone added new `npm` packages):

```bash
$ yarn install
$ npx jetify
```

### Android

There are three build variants on Android. The first variant, `debug`, is the
app itself without any JavaScript assets bundled. The second variant, `release`
is the app that ready for publishing on Google Play Store. The third variant,
`review` is a `debug` variant but bundled with JavaScript assets, thus allows
the app run standalone in the device.

The Android app build process requires a Java Keystore (JKS) containing the
public-private keys for signing the `review` and `release` variants. Please
contact the maintainer to obtain the JKS file or create a new one if building
the app for different conference/event.

Before building a build variant, prepare the properties for authenticating
to the JKS in the Android source set beforehand:

```bash
$ cd android/keystores
$ touch review.keystore.properties
$ echo storePassword=KEYSTORE_PASS >> review.keystore.properties
$ echo keyPassword=KEYSTORE_PASS >> review.keystore.properties
$ echo keyAlias=qir-review >> review.keystore.properties
$ echo storeFile=KEYSTORE_LOCATION >> review.keystore.properties
$ touch release.keystore.properties
$ echo storePassword=KEYSTORE_PASS >> release.keystore.properties
$ echo keyPassword=KEYSTORE_PASS >> release.keystore.properties
$ echo keyAlias=qir-release >> release.keystore.properties
$ echo storeFile=KEYSTORE_LOCATION >> release.keystore.properties
```

> Note: Ask the maintainer(s) to obtain the actual value for `KEYSTORE_PASS`

To generate a build variant, e.g. `review`, invoke the corresponding `assemble`
Gradle task. Example:

```bash
$ cd android
$ ./gradlew assembleReview
$ cd app/build/outputs/apk/review
$ adb install app-review.apk
```

## Maintainers & Contributors

- [Daya Adianto](https://gitlab.com/addianto)
- [Danny August](https://gitlab.com/daystram)
- [Ganda Fitrandia](https://gitlab.com/ganfitran)
- [Meitya Dianti](https://gitlab.com/meityad)
- [Ade Azurat](https://gitlab.com/adeazurat)

## License

Copyright (C) 2019 Daya Adianto. Developed at [Pusilkom UI](http://pusilkom.ui.ac.id)
for [QiR Conference](https://qir.eng.ui.ac.id) organised by
[Faculty of Engineering Universitas Indonesia](http://eng.ui.ac.id).

This project is licensed under [GPLv3](LICENSE). The initial codebase is based
on [ICAS-PGS](https://gitlab.com/addianto/icas-pgs) app that licensed under
[Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0.html).
